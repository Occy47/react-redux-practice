import { createSelector } from 'reselect'
import { sortBy } from 'lodash';

const MOVIE_SORTS = {
    SORT_BY_TITLE: movies => sortBy(movies, 'title'),
    SORT_BY_RATING: movies => sortBy(movies, 'rating'),
    SORT_BY_DURATION: movies => sortBy(movies, 'duration'),
};

function getMoviesAsIdsSelector(state) {
    let unSortedMovies = {};
    let sortedMovies = [];

    unSortedMovies = state.movieState.ids
        .map(id => state.movieState.entities[id])
    sortedMovies = MOVIE_SORTS[state.sortByState](unSortedMovies);

    return sortedMovies.map(movie => movie.id);
}

function getNotificationsSelector(state) {
    return getArrayOfObject(state.notificationState);
}

function getArrayOfObject(object) {
    return Object.keys(object).map(key => object[key]);
}

const getMoviesAsIds = createSelector(getMoviesAsIdsSelector, m => m);
const getNotifications = createSelector(getNotificationsSelector, n => n);

export  {
    getMoviesAsIds,
    getNotifications,
};