import types from './types';

function doAddMovie(id, newMovie) {
    const { title, duration, rating } = newMovie;
    return {
        type: types.MOVIE_ADD,
        movie: { id, title, duration, rating },
    };
}

function doRemoveMovie(id) {
    return {
        type: types.MOVIE_REMOVE,
        id,
    };
}

function doSortBy(sorter) {
    return {
        type: types.MOVIE_SORT_BY,
        sorter,
    };
}

function doHideNotification(id) {
    return {
        type: types.NOTIFICATION_HIDE,
        id
    };
}

function doAddMovieWithNotification(id, newMovie) {
    const {title, duration, rating} = newMovie;
    return {
        type: types.MOVIE_ADD_WITH_NOTIFICATION,
        movie: {id, title, duration, rating},
    }
}

export {
    doRemoveMovie,
    doAddMovie,
    doSortBy,
    doHideNotification,
    doAddMovieWithNotification
}



