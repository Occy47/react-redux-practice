import React from 'react';
import ReactDOM from 'react-dom';
import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { Provider } from 'react-redux';
import VideoStoreApp from './components/VideoStoreApp';
import rootReducer from './reducers'
import rootSaga from "./sagas/index.js";

const logger = createLogger();
const saga = createSagaMiddleware();

const store = createStore(
    rootReducer,
    undefined,
    applyMiddleware(saga, logger)
    );

saga.run(rootSaga);

ReactDOM.render(
    <Provider store={store}>
        <VideoStoreApp />
    </Provider>,
    document.getElementById('root'));

