import React from 'react';
import ReactDOM from 'react-dom';
import VideoStoreApp from './VideoStoreApp';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<VideoStoreApp />, div);
  ReactDOM.unmountComponentAtNode(div);
});
