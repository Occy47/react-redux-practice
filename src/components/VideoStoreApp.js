import React, { Component } from 'react';
import ConnectedMovieList from '../containers/MovieList';
import ConnectedAddMovieForm from '../containers/AddMovieForm';
import ConnectedNotifications from '../containers/Notifications';

class VideoStoreApp extends Component {
  render() {
    return (
      <div>
        <header>
          <h2>Video store</h2>
          <ConnectedAddMovieForm/>
          <ConnectedMovieList/>
          <ConnectedNotifications/>
        </header>
      </div>
    );
  }
}

export default VideoStoreApp;
