import React from 'react';

function Input({ label, onChange, placeHolder }) {
    return (
        <div>
            <label>{label}</label>
            <input type='text' onChange={onChange} placeholder={placeHolder}></input>
        </div>

    );
}

export default Input;