import types from '../actions/types';

function notificationReducer(state = {}, action) {
    switch (action.type) {
        case types.MOVIE_ADD: {
            return applySetNotifyAboutAddMovie(state, action);
        }
        case types.NOTIFICATION_HIDE: {
            return applyRemoveNotification(state, action);
        }
        default:
            return state;
    }
}

function applySetNotifyAboutAddMovie(state, action) {
    const { id, title } = action.movie;
    return { ...state, [id]: 'Movie added: ' + title };
}

function applyRemoveNotification(state, action) {
    const {
        [action.id]: notificationToRemove,
        ...restNotifications
    } = state;
    return restNotifications;
}

export default notificationReducer;