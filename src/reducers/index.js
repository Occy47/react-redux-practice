import { combineReducers } from 'redux';
import movieReducer from './movieReducer';
import sortReducer from './sortReducer';
import notificationReducer from './notificationReducer';

export default combineReducers({
    movieState: movieReducer,
    sortByState: sortReducer,
    notificationState: notificationReducer,
});
