import types from '../actions/types';
import { schema, normalize } from 'normalizr';
import uuid from 'uuid/v4';

const movies = [
    { id: uuid(), title: 'Conan the barbarian', duration: '1:56', rating: '9.5' },
    { id: uuid(), title: 'The Matrix', duration: '1:47', rating: '9.6' },
    { id: uuid(), title: 'Predator', duration: '1:22', rating: '9.8' },
];

const movieSchema = new schema.Entity('movie');

const normalizedMovies = normalize(movies, [movieSchema]);

const initialMovieState = {
    entities: normalizedMovies.entities.movie,
    ids: normalizedMovies.result,
};
console.log(initialMovieState);
function movieReducer(state = initialMovieState, action) {
    switch (action.type) {
        case types.MOVIE_ADD: {
            return applyAddMovie(state, action);
        }
        case types.MOVIE_REMOVE: {
            return applyRemoveMovie(state, action);
        }
        default:
            return state;
    }
}

function applyAddMovie(state, action) {
    const movie = { ...action.movie };
    const entities = { ...state.entities, [movie.id]: movie };
    const ids = [...state.ids, action.movie.id];
    return { ...state, entities, ids }; 
}

function applyRemoveMovie(state, action) {
    const id = action.id;
    const ids = state.ids.filter(movieId => movieId !== id);
    let entities = {};

    ids.forEach(id => { entities[id] = state.entities[id]; });

    return { ...state, entities, ids };
}

export default movieReducer;