import types from '../actions/types';

function sortReducer(state = 'SORT_BY_TITLE', action) {
    switch (action.type) {
        case types.MOVIE_SORT_BY: {
            return applySortBy(state, action);
        }
        default:
            return state;
    }

}

function applySortBy (state, action) {
    return action.sorter;
}

export default sortReducer;