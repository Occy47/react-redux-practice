import { delay } from 'redux-saga';
import { put, takeEvery } from 'redux-saga/effects';
import types from '../actions/types';
import { doAddMovie, doHideNotification } from '../actions';

function* watchAddMovieWithNotification() {
    yield takeEvery(types.MOVIE_ADD_WITH_NOTIFICATION, handleAddMovieWithNotification);
}

function* handleAddMovieWithNotification(action) {
    const movie = action.movie;
    const id = action.movie.id;
    yield put(doAddMovie(id, movie));
    yield delay(2500);
    yield put(doHideNotification(id));
}

export default watchAddMovieWithNotification;