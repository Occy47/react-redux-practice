import React from 'react';
import { connect } from 'react-redux';
import { getNotifications } from '../selectors/index.js';

function Notifications({ notifications }) {
    return (
        <div>
            {notifications.map(note => <div key={note}>{note}</div>)}
        </div>
    )
}

function mapStateToProps(state, props) {
    return {
        notifications: getNotifications(state),
    };
}

const ConnectedNotifications = connect(mapStateToProps)(Notifications);

export default ConnectedNotifications;