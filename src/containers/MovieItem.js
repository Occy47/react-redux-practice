import React from 'react';
import { doRemoveMovie } from '../actions/index.js'
import { connect } from 'react-redux';

function MovieItem({ movie, onRemoveMovie }) {
    const { id, title, duration, rating } = movie;
    return (

        <tr>
            <td>
                {title}
            </td>
            <td>
                {duration}
            </td>
            <td>
                {rating}
            </td>
            <td onClick={() => onRemoveMovie(id)}>delete</td>
        </tr>
    );
}

function mapStateToProps(state, props) {
    return {
        movie: state.movieState.entities[props.movieId],
    };
}

function mapDispatchToProps(dispatch) {
    return {
        onRemoveMovie: (id) => dispatch(doRemoveMovie(id)),
    };
}

const ConnectedMovieItem = connect(mapStateToProps, mapDispatchToProps)(MovieItem);

export default ConnectedMovieItem;