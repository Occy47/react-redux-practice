import React from 'react';
import ConnectedMovieItem from './MovieItem';
import { getMoviesAsIds } from '../selectors/index.js';
import { connect } from 'react-redux';
import { doSortBy } from '../actions';

function MovieList({ moviesAsIds, onSortBy }) {

    const movieList = moviesAsIds.map(movieId =>
        <ConnectedMovieItem key={movieId} movieId={movieId} />
    );

    return (
        <table>
            <thead>
                <tr>
                    <th onClick={() => onSortBy('SORT_BY_TITLE')}>Title</th>
                    <th onClick={() => onSortBy('SORT_BY_DURATION')}>Duration</th>
                    <th onClick={() => onSortBy('SORT_BY_RATING')}>Rating</th>
                </tr>
            </thead>
            <tbody>
                {movieList}
            </tbody>
        </table>
    );
}

function mapStateToProps(state) {
    return {
        moviesAsIds: getMoviesAsIds(state),
    };
}

function mapDispatchToProps(dispatch) {
    return {
        onSortBy: sortType => dispatch(doSortBy(sortType))
    };
}

const ConnectedMovieList = connect(mapStateToProps, mapDispatchToProps)(MovieList);

export default ConnectedMovieList;