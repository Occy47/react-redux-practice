import React, { Component } from 'react';
import Input from '../components/Input'
import { doAddMovieWithNotification } from '../actions/index.js'
import { connect } from 'react-redux';
import uuid from 'uuid/v4';

class AddMovieForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            duration: '',
            rating: '',
        };
        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleDurationChange = this.handleDurationChange.bind(this);
        this.handleRatingChange = this.handleRatingChange.bind(this);
    }

    handleTitleChange(event) {
        this.setState({ title: event.target.value });
    }
    handleDurationChange(event) {
        this.setState({ duration: event.target.value });
    }
    handleRatingChange(event) {
        this.setState({ rating: event.target.value });
    }

    render() {
        const { onAddMovie } = this.props;
        const { title, duration, rating } = this.state;

        const movie = { title, duration, rating };
        return (
            <div>
                <h3>Add Movie</h3>
                <Input label='Movie title: ' onChange={this.handleTitleChange} placeHolder='Enter title'></Input>
                <Input label='Movie duration: ' onChange={this.handleDurationChange} placeHolder='Enter duration'></Input>
                <Input label='Movie rating: ' onChange={this.handleRatingChange} placeHolder='Enter rating'></Input>
                <button onClick={() => onAddMovie(movie)}>Submit movie</button>
            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onAddMovie: movie => dispatch(
            doAddMovieWithNotification(uuid(),movie)),
    };
}

const ConnectedAddMovieForm = connect(null, mapDispatchToProps)(AddMovieForm);

export default ConnectedAddMovieForm;